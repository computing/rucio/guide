---
title: k8s GWF walkthrough
---


# Online registration walkthrough

!!! warning "under construction"

    This is a stub

Steps to register a new dataset:

1. If new scope, pick a scope name (link to scopes conventions docs)
1. Create scope: `rucio-admin scope add --account frames --scope frames.L1-hoft.O4`
1. If frame type not defined in LFN2PFN algorithm, update conventions table: <https://git.ligo.org/computing/rucio/gravcat-policy/-/blob/overhaul-ligo-lfn2pfn/gravcat_policy/data/ldas_frame_conventions.yaml>
1. If updating lfn2pfn algorithm, rebuild server, daemons and client containers.
1. Restart server and daemons with e.g. `kubectl create job daemons-restart --from=cronjob/rucio-daemons-automatic-restart`
1. Set up gravcat config file
1. Set up gravcat deployment manifest
1. Add config and manifest to kustomization file
1. deploy with `kubectl apply -k .`
