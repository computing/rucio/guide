---
title: Dataset replication: moving and managing datasets
---

# Dataset replication

## Replication rules

## Subscription-based rule creation

!!! example "Subscribe RSE LIGO-LA to `H1_HOFT_C00_AR` frame files"

    Create subscription:

    ```shell
    $  rucio-admin subscription add --account frames O4-LIGO-LA-hoft-ar \
        '{"stream_name": ["H1_HOFT_C00_AR"], "did_type": ["DATASET"]}' \
        '[{"copies": 1, "rse_expression": "LIGO-LA", "activity": "User Subscriptions"}]' \
        'Subscribe LIGO-LA to H1_HOFT_C00_AR'
    Subscription added f4b9ccab1f784ee280d269c0e7de4901
    ```

    Re-evaluate existing datasets to generate new rules (rules will be generated for new datasets as they are created):

    ```shell
    $  rucio-admin subscription reevaluate frames.H1-hoft.O4:H-H1_HOFT_C00_AR-136,frames.H1-hoft.O4:H-H1_HOFT_C00_AR-137
    ```

    Check rules creation:

    ```shell
    $  rucio list-rules --account frames | grep LIGO-LA
    a5049d18330b4e458606136f8702c565  frames     frames.H1-hoft.O4:H-H1_HOFT_C00_AR-136  REPLICATING[0/152/0]    LIGO-LA           1                          2023-09-21 19:54:13
    7b381d63840b429cbdbfc72e8ff426e8  frames     frames.H1-hoft.O4:H-H1_HOFT_C00_AR-137  REPLICATING[0/1981/0]   LIGO-LA           1                          2023-09-21 19:54:14
    ```

