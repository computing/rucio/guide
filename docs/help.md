# Help and support

See <https://computing.docs.ligo.org/guide/help/>

## Feedback on this site {: #feedback }

If you would like to suggest an improvement to this site:

[Click here to create an issue](https://git.ligo.org/computing/rucio/guide/-/issues/new){: .md-button}

All pages on this site also have an edit button styled as a pencil
(:material-pencil:), simply click on that button to navigate to the source
markdown for that page, where you can propose an edit to the page.

Notes:

-   Please review the [contributing guide](https://git.ligo.org/computing/rucio/guide/-/blob/main/CONTRIBUTING.md) before submitting your edits.
-   When proposing edits using the pencil (:material-pencil:) links, please
    make sure that you change the _Target Branch_ option on the web editor
    to something specific to your change (e.g. `update-htcondor-docs`) and
    ensure that the :material-checkbox-blank:
    _Start a new merge request with these changes_ checkbox is
    __checked__ (:material-checkbox-marked:).
    This will ensure that your change can be reviewed before being accepted.
