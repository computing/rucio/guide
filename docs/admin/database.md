---
title: Rucio database
---

# Rucio database operations

## Deployment

WIP

## Backups

!!! tip "Basic commands: `pg_dump` & `pg_restore`"

    Backup to tar format:
    ```shell
    # pg_dump -U rucio rucio --host rucio-db-postgresql \
        -f /tmp/rucio-$(date +%Y%m%d-%H0000).tar -F t
    ```

    Restore from tar format into a new postgres host 
    `rucio-db-new-postgresql`:
    ```shell
    # pg_restore -U rucio -d rucio -h rucio-db-new-postgresql \
        /tmp/rucio-20240229-190000.tar
    ```

!!! info "Using independent postgres client pods"

    In the examples below we carry out `pg_dump`/`pg_restore` operations in
    separate client pods which are configured to connect to the main rucio
    postgres database.  This is not a hard requirement; the basic operations
    could be carried out in the main postgres pods themselves.  Using the
    client pods simply maps more cleanly to automated procedures.

### Manual backup

Launch a postgres client pod:

```shell
$  kubectl run postgresql-client-rucio \
        --rm --tty -i --restart='Never' --namespace ligo-rucio \
        --image bitnami/postgresql --env="PGPASSWORD=REDACTED" \
        --command -- psql --host rucio-db-postgresql -U rucio
```

Note that the host is simply the name of the postgres svc as everything is
running in the same namespace on the same k8s cluster.  

This lands in a interactive postgres session so kubectl-exec from a new
termainal to shell in to that pod:

```shell
$  kubectl exec postgresql-client-rucio -- /bin/bash
```

Now execute `pg_dump` in the pod:

```shell
# pg_dump -U rucio rucio --host rucio-db-postgresql \
    -f /tmp/rucio-prod-$(date +%Y%m%d-%H0000).tar -F t
```

You may want compress the tarball with e.g. `gzip` (`bzip` is not availiable in
the postgres container).

Copy the database dump from the pod (remember to keep the pod alive as we
have not mounted a PVC in this example) to your local host:

```shell
$  kubectl cp postgres-client-rucio:/tmp/rucio-20240306-180000.tar \
        rucio-20240306-180000.tar
```

You may now remove the client pod:

```shell
$  kubectl delete pod postgres-client-rucio
```


### Automated/scheduled backup

WIP, plan:

1. k8s cronjob to execute `pg_dump` and upload result to NRP S3 bucket.
1. Conodr cronjob(s) to pull db dump down to CIT and other locations.

## Restoration / migration

This assumes the existence of a gzipped tar-format database dump from an
existing rucio database, as documented above.

Deploy a new postgres instance with Helm:

```shell
$   helm install rucio-db-new bitnami/postgresql -f ./values.yaml
```

See
[`postgres_values.yaml`](https://git.ligo.org/computing/rucio/igwn-rucio-deploy/-/blob/75915d059e05458faf42968884c29493fb51d424/postgres_values.yaml)
for example Helm values appropriate for rucio on NRP.

Launch a postgres client pod to connect to the NEW postgres deployment
`rucio-db-new-postgresql` (make sure there are no concurrently running client
pods with the same name):

```shell
$  kubectl run postgresql-client-rucio \
        --rm --tty -i --restart='Never' --namespace ligo-rucio \
        --image bitnami/postgresql --env="PGPASSWORD=REDACTED" \
        --command -- psql --host rucio-db-new-postgresql -U rucio
```

Copy the database dump tarball to the client pod:

```shell
$  kubectl cp ./rucio-20240306-180000.tar.gz postgresql-client-rucio:/tmp/rucio-20240306-180000.tar.gz
```

This lands in a interactive postgres session so kubectl-exec from a new
termainal to shell in to that pod as above:

```shell
$  kubectl exec postgresql-client-rucio -- /bin/bash
```

Finally, in the client pod, decompress the tarball:

```shell
#  gunzip /tmp/rucio-20240306-180000.tar.gz
```

Now restore using `pg_restore`:

```shell
#  pg_restore -U rucio -d rucio -h rucio-db-new-postgresql /tmp/rucio-20240306-180000.tar
```

In an interactive postgres session, check the database in the new postgres
instance has successfully migrated with e.g.:

```sql 
rucio=> SET search_path='prod';
SET
rucio=> SELECT COUNT(*) FROM dids;
 count
--------
 378670
(1 row)
```

## Rucio database schema upgrade

Major releases for Rucio (typically) require an upgrade of the database schema.
See the [rucio database upgrade
docs](https://rucio.cern.ch/documentation/operator/database/#upgrading-and-downgrading-the-database-schema).
for basic details.  This section has some tips to ensure this procedure goes
smoothly.

!!! danger "Test schema upgrade on non-production database"

    Upgrading the rucio database schema alters tables in-situ and has been
    known to completely break a deployment.  Use the backup/restore steps on
    this page to deploy a test instance of the rucio postgresql database where
    you can do a dry run of the schema upgrade.

!!! tip "Ensure alembic version table is correctly populated"

    Several organisations (including but not necessarily limited to LIGO, Virgo
    and Xenon1T) have observed that the Alembic version table fails to get
    populated during rucio database initialisation.  The current alembic version
    is required for successful schema upgrades.

    Log into the database and check the `alembic_version` table:

    ```sql
    rucio=> SET search_path='prod';
    SET
    rucio=> SELECT version_num FROM alembic_version;
     version_num
    -------------
    (0 rows)
    ```

    If necessary, insert the missing value for the *current* database alembic
    version:
    
    ```sql
    rucio=> INSERT INTO alembic_version(version_num) VALUES ('27e3a68927fb');
    INSERT 0 1
    ```

!!! tip "Determining the correct alembic version"

    In the event you need to populate the `alembic_version` table manually, the
    correct version for your *current* deployment can be found in the file
    `lib/rucio/alembicrevision.py` of the rucio source repository and tag which
    corresponds to the rucio version you are running.   

    Alternatively, examine the installed copy of that file on your rucio server
    (prior to upgrading the server) at the path like:
    `/usr/local/lib/python3.9/site-packages/rucio/alembicrevision.py`.

Alembic upgrades should be run on a `rucio-server` instance at the desired
upgrade version with a copy of the `/opt/rucio/etc/alembic.ini` file which
should include lines like:

```cfg
sqlalchemy.url=postgresql://rucio:REDACTED@rucio-db-postgresql/rucio
version_table_schema=prod

# Path to the Alembic migration scripts
script_location = /usr/local/lib/python3.9/site-packages/rucio/db/sqla/migrate_repo/
```

To see the changes that will be applied by upgrading to the HEAD
(note the differences with the [upstream
docs](https://rucio.cern.ch/documentation/operator/database/#upgrading-and-downgrading-the-database-schema)):

```shell
# alembic -c /opt/rucio/etc/alembic.ini upgrade --sql $(alembic \
        -c /opt/rucio/etc/alembic.ini current | cut -d' ' -f1):head
```


Apply the upgrade with:

```shell
# alembic -c /opt/rucio/etc/alembic.ini upgrade head
```

Before upgrade:

```sql
rucio=> SELECT * FROM alembic_version;
 version_num
--------------
 2190e703eb6e
(1 row)
```

After upgrade:

```
rucio=> SELECT * FROM alembic_version;
 version_num
--------------
 27e3a68927fb
```


