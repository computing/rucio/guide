---
title: Rucio on NRP
---

# Nautilus k8s cluster

The LIGO Rucio instance is deployed on the __Nautilus__ Kubernetes (k8s)
cluster, part of the [__National Research Platform
(NRP)__](https://nationalresearchplatform.org/).  Familiarity with basic k8s
usage is assumed for this documentation.  Please see the [__Nautilus__
documentation](https://docs.nationalresearchplatform.org/) for k8s tutorials
and usage guides.  The [Nautilus quick start
guide](https://docs.nationalresearchplatform.org/userdocs/start/quickstart/)
is particularly useful.  For more introductory documentation see e.g.
<https://kubernetes.io/docs/tutorials/kubernetes-basics/>.

!!! info "NRP Nautilus portal"

    The primary administrative interface for access control in Nautilus is the
    [__NRP Nautilus portal__](https://portal.nrp-nautilus.io/), accessible via
    CILogon credentials from the LIGO Scientific Collaboration identiy
    provider.  Use the portal to download your `kube` configuration file, add
    users and namespaces, and see available resources.

## Accessing the `ligo-rucio` k8s namespace

The LIGO Rucio instance is deployed in the `ligo-rucio` namespace.  To gain
user or admin access to this namespace, follow the Nautilus [__access
instructions__](https://docs.nationalresearchplatform.org/userdocs/start/get-access/):

1. Log into the [__NRP Nautilus portal__](https://portal.nrp-nautilus.io/) to
   become a guest user on Nautilus.
1. Contact a `ligo-rucio` administrator (<james.clark@ligo.org>) to be promoted
   to `user` or `admin` access.

!!! info "Nautilus support"

    The primary support channel for Nautilus is the federated communication
    system <https://matrix.org> (analogous to Slack et al).  See the
    [__Nautilus
    docs__](https://docs.nationalresearchplatform.org/userdocs/start/contact/)
    for information on using this system.

## Monitoring k8s resource usage

Nautilus has extensive Grafana-based monitoring at
<https://grafana.nrp-nautilus.io/>.  Of particular note are:

* The `ligo-rucio` _workload_ resource consumption (`deployments`, `jobs` and `statefulsets`)
  [dashboard](https://grafana.nrp-nautilus.io/d/a87fb0d919ec0ea5f6543124e16c42a5/kubernetes-compute-resources-namespace-workloads?orgId=1&refresh=10s&var-datasource=default&var-cluster=&var-namespace=ligo-rucio&var-type=deployment).
* The `ligo-rucio` _pod_ resource consumption
  [dashboard](https://grafana.nrp-nautilus.io/d/85a562078cdf77779eaa1add43ccec1e/kubernetes-compute-resources-namespace-pods?var-datasource=default&var-cluster=&var-namespace=ligo-rucio&orgId=1&refresh=10s).

Where necessary, use these dashboards to determine appropriate resource
requests in k8s manifests in line with [Nautilus
policies](https://docs.nationalresearchplatform.org/userdocs/start/policies/).
