---
title: Deployment walkthrough
---

# LIGO Rucio deployment walkthrough {: #walkthrough }

This page gives a detailed walkthrough of the steps necessary to stand up and
initialise the LIGO Rucio deployment, beginning from an empty k8s namespace.
Familiarity with the role of each compontent is assumed.

All major Rucio and PostgreSQL components use upstream Helm charts with
additional auxillary k8s objects and use-case specific configurations.  Please
see <https://helm.sh/docs/intro/> for an introduction to the Helm package
manager.

!!! info "Configuration files source location"
    All operations documented on this page can be carried out via the shell
    scripts, standalone k8s jobs and Helm values files contained in the [IGWN
    Rucio Deployment
    Gitlab project](https://git.ligo.org/computing/rucio/igwn-rucio-deploy).

Follow the sections _in the order they are presented_ to stand up a new Rucio
instance.

## PostgreSQL database {: #database }

Rucio is underpinned by a backend SQL database.  PostgreSQL, MySQL and Oracle
are all supported.  We use PostgreSQL 12, deployed in k8s with the _basic_
[Bitnami Helm
chart](https://github.com/bitnami/charts/tree/main/bitnami/postgresql).

!!! warning "Single pod PostgreSQL configuration"

    For simplicity and easy compatibility with Rucio, we currenty opt to use a
    very basic PostgreSQL configuration.  Other high-availability options,
    particularly with automated backup should be explored in the future.  For
    now, state is preserved by means of persistent volumes on the Nautilus
    CephFS and reliable uptime is achieved by targeting highly stable hosts at
    UCSD.

!!! tip "PostgreSQL helper script"

    The
    [`helm_install_postgres`](https://git.ligo.org/computing/rucio/igwn-rucio-deploy/-/blob/main/helm_install_postgres)
    helper script is intended as a helpful tool to simplify / document the
    PostgreSQL database installation and updates with Helm.

To deploy and set up the Rucio database:

1. Configure database name and users:

    ```shell
    $  bootstrap_identity=<desired-init-username> # E.g. "bootstrap"
    $  bootstrap_pwd=<desired-init-password> # E.g. "rucio-bootstrap"
    $  rucio_db_pwd=<desired-database-password> # E.g. "rucio"
    $  rucio_db_user=<desired-database-username> # E.g. "rucio"
    $  rucio_db_name=<desired-database-name> # E.g. "rucio"
    $  rucio_db_schema=<desired-schema-name> # E.g. "prod"
    $  cat << EOF | kubectl apply -f -
    apiVersion: v1
    data:
      rucio-cfg-database-default: $(echo postgresql://${rucio_db_user}:${rucio_db_pwd}@${postgres_deploy_name}-postgresql/${rucio_db_name} | base64)
      rucio-db-user: $(echo ${rucio_db_name} | base64)
      rucio-db-name: $(echo ${rucio_db_user} | base64)
      rucio-db-schema: $(echo ${rucio_db_schema} | base64)
      rucio-db-pwd: $(echo ${rucio_db_pwd} | base64)
      rucio-db-bootstrap-identity: $(echo ${bootstrap_identity} | base64)
      rucio-db-bootstrap-pwd: $(echo ${bootstrap_pwd} | base64)
      rucio-db-hostname: $(echo ${postgres_deploy_name}-postgresql | base64)
    kind: Secret
    metadata:
      name: rucio-db-init-secrets
    type: Opaque
    EOF
    ```

2. Configure database initalisation script and store as a secret (which will get mounted and executed at install time):

    ```shell
    $  create_rucio_db=$(echo "CREATE DATABASE ${rucio_db_name};")
    $  create_rucio_user=$(echo "CREATE USER ${rucio_db_user} WITH ENCRYPTED PASSWORD '${rucio_db_pwd}';")
    $  grant_user_privileges=$(echo "GRANT ALL PRIVILEGES ON DATABASE ${rucio_db_name} to ${rucio_db_user};")
    $  cat << EOF | kubectl apply -f -
    apiVersion: v1
    data:
      create-rucio-db.sql: $(echo ${create_rucio_db} | base64)
      create-rucio-user.sql: $(echo ${create_rucio_user} | base64)
      grant-user-privileges.sql: $(echo ${grant_user_privileges} | base64)
    kind: Secret
    metadata:
      name: postgres-initdb-scripts
    type: Opaque
    EOF
    ```

3. Configure PostgreSQL with a `values.yaml` file.  Currently deployed values
    can be found in
    [`postgres_values.yaml`](https://git.ligo.org/computing/rucio/igwn-rucio-deploy/-/blob/main/postgres_values.yaml)
    but a minimal configuration might look like:

    ```yaml
    image:
      registry: docker.io
      repository: bitnami/postgresql
      tag: 12
      debug: true
    primary:
      initdb:
        scriptsSecret: 'postgres-initdb-scripts'
      persistence:
        enabled: True
        size: 16Gi
      resources:
        limits:
          memory: "8Gi"
          cpu: "2"
        requests:
          memory: "4Gi"
    ```

4. Install the Helm chart:

```shell
$ helm install rucio-db bitnami/postgresql -f values.yaml
```

!!! info "Database is not directly exposed to the world"

    Since the database, and all Rucio components which need to communicate with
    it, are deployed in k8s, we do not ever expose the database to the outside
    world:  there is no way for a malicious actor to gain direct access to the
    database without first gaining access to the Nautilus `ligo-rucio`
    namespace.

### Bootstrap database {: #bootstrap }

Finally, we bootsrap the database using a k8s job and the
[`rucio-init`](https://github.com/rucio/containers/tree/master/init) Docker
container.  This creates the necessary tables and adds a root account with the
configured identities.

The bootstrap k8s job is defined in
[`init-db-schema.yaml`](https://git.ligo.org/computing/rucio/igwn-rucio-deploy/-/blob/main/init-db-schema-job.yaml).
Launch the job with:

```shell
$  kubectl apply -f init-db-schema-job.yaml
job.batch/rucio-init-db created
```

## CA-bundle k8s secret {: #cabundler }

Some functions on the server and in the daemons require certificates and CAs to
work.  For full details see the
[`rucio-daemons`](https://github.com/rucio/helm-charts/tree/master/charts/rucio-daemons)
and
[`rucio-server`](https://github.com/rucio/helm-charts/tree/master/charts/rucio-server)
directories in the [Rucio Helm chart repository](https://github.com/rucio/helm-charts).

For our purposes, we simply create the necessary secrets, documented in the
links above, from the entire `osg-ca-certs` RPM.  This process is automated via
another (manually executed) k8s job (defined in
[`ca-bundler-job.yaml`](https://git.ligo.org/computing/rucio/igwn-rucio-deploy/-/blob/main/ca-bundler-job.yaml))
which executes the
[`ca-bundler`](https://git.ligo.org/computing/rucio/containers/-/tree/main/ca-bundler)
Docker container.

First, create a service account and rolebinding which we will use in a
subsequent k8s job:

```shell
$  kubectl apply -f rucio-kubectl-service-account.yaml
```

Now create the ca-bundles with the job described in `ca-bundler-job.yaml`:

```shell
$  kubectl apply -f ca-bundler-job.yaml
job.batch/rucio-ca-bundler created
```

## Rucio server {: #server }

!!! tip "Server helper script"

    The
    [`helm_install_server`](https://git.ligo.org/computing/rucio/igwn-rucio-deploy/-/blob/main/helm_install_server)
    helper script is intended as a helpful tool to simplify / document the
    Rucio server installation and updates with Helm.

!!! danger "Use Helm charts < version 1.31.x"

    Version 1.31.x of the `rucio-servers` helm chart is incompatible with old
    values files.  At time of writing the `rucio-servers` Helm installation
    uses `rucio-server-1.30.13`and a 1.30.x series values.  See [rucio release
    notes](https://github.com/rucio/rucio/releases/tag/1.31.0) for more
    details.

1. Calls from the API server to FTS use X.509 certificate authentication (see
   [rucio-server Helm
   charts](https://github.com/rucio/helm-charts/tree/master/charts/rucio-server)).
   Create secrets for a server certificate and key as follows:

    ```shell
    #  Assume following usercert:
    $  openssl x509 -in usercert.pem -subject -noout -nameopt compat
    subject=/DC=org/DC=incommon/C=US/ST=California/O=California Institute of Technology/CN=rucio.ligo.caltech.edu
    $  USERCERT=$(base64 -w0 usercert.pem) # secrets require base64-encoding
    $  cat << EOF | kubectl apply -f -
    apiVersion: v1
    data:
      usercert.pem: ${USERCERT}
    kind: Secret
    metadata:
      name: ${server_deploy_name}-fts-cert
    type: Opaque
    EOF

    $  USERKEY=$(base64 -w0 userkey.pem) # secrets require base64-encoding
    $  cat << EOF | kubectl apply -f -
    apiVersion: v1
    data:
      new_userkey.pem: ${USERKEY}
    kind: Secret
    metadata:
      name: ${server_deploy_name}-fts-key
    type: Opaque
    EOF
    ```

2. Install the server helm chart with e.g.:

    ```shell 
    $  helm install rucio-server rucio/rucio-server -f values.yaml \
     --set config.database.default=postgresql://rucio:secret@rucio-db-postgresql/rucio \
     --set config.database.schema=prod \
     --version 1.30.13
    ```
where the config values at the command line should match those from the
[database deployment](#database).  The remaining _current_ configuration values
used in the Helm chart can be found in
[`server_values.yaml`](https://git.ligo.org/computing/rucio/igwn-rucio-deploy/-/blob/main/server_values.yaml).

3. Finally, to allow the server pods to start, we need an X.509 proxy generated
   from the cert/key pair above.  Initialise the proxy by launching a one-off
   job from the Helm-generated cronjob used to refresh that proxy:

    ```shell
    $  kubectl create job server-proxy --from=cronjob/rucio-server-renew-fts-proxy
    job.batch/server-proxy created
    ```

## Rucio daemons {: #daemons }

!!! tip "Daemons helper script"

    The
    [`helm_install_daemons`](https://git.ligo.org/computing/rucio/igwn-rucio-deploy/-/blob/main/helm_install_daemons)
    helper script is intended as a helpful tool to simplify / document the
    Rucio daemons installation and updates with Helm.

1. As above, some daemons require certificates to communicate with FTS (see
   [rucio-daemons Helm
   charts](https://github.com/rucio/helm-charts/tree/master/charts/rucio-daemons)).
   Create secrets for the daemons certificate and key as follows:

    ```shell
    $  USERCERT=$(base64 -w0 secrets/usercert.pem)
    $  cat << EOF | kubectl apply -f -
    apiVersion: v1
    data:
      usercert.pem: ${USERCERT}
    kind: Secret
    metadata:
      name: ${daemons_deploy_name}-fts-cert
    type: Opaque
    EOF

    $  USERKEY=$(base64 -w0 secrets/userkey.pem)
    $  cat << EOF | kubectl apply -f -
    apiVersion: v1
    data:
      new_userkey.pem: ${USERKEY}
    kind: Secret
    metadata:
      name: ${daemons_deploy_name}-fts-key
    type: Opaque
    EOF
    ```

2. If the [automatix
   daemon](https://rucio.github.io/documentation/bin/rucio-automatix) is
   enabled in the daemons' values file (see the [Monitoring](./monit.md)
   documentation for more details about automatix), we also require a
   secret to hold that daemon's configuration:

    ```shell
    $  kubectl create secret generic ${daemons_deploy_name}-automatix-input \
     --save-config \
     --dry-run=client \
     --from-file=automatix.json \
     -o yaml | \
     kubectl apply -f -
    ```

3. Install the daemons helm chart (note that we only need the client
   configuration if using the Automatix daemon):
    ```shell
    $  helm install rucio-daemons rucio/rucio-daemons -f values.yaml \
      --set config.database.default=postgresql://rucio:secret@rucio-db-postgresql/rucio \
      --set config.database.schema=prod \
      --set config.client.rucio_host=http://rucio-server --set config.client.auth_host=http://rucio-server-auth \
      --set config.client.ca_cert=/etc/pki/tls/certs/ca-bundle.crt \
      --set config.client.auth_type=userpass \
      --set config.client.username=admin \
      --set config.client.password=supersecretpassword \
      --version 1.30.12
    ```

  
__Congratulations__: you should now have a fully functional Rucio installation!

Continue to the [database](./database.md) page to learn more details about
specific components' configurations or [skip to the configuration
pages](../config/index.md) to immediately start configuring your new Rucio
instance for LIGO.
