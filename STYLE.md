# Style and syntax rules for the IGWN Computing Guide

## Syntax rules

The IGWN Computing Guide uses
[Markdown](https://daringfireball.net/projects/markdown/)
for source formatting, and
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
for building, so all source content must follow the syntax rules
of those tools.

In particular, see the following pages for useful syntax guides:

-   <https://daringfireball.net/projects/markdown/syntax>

    For the original syntax definition for Markdown.

-   <https://squidfunk.github.io/mkdocs-material/reference/>

    For details of the extra concepts that Material for MkDocs
    introduces on top of the standard markdown, e.g.
    [admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/#usage).

## Style rules

This project uses [`markdownlint`](https://github.com/DavidAnson/markdownlint)
for static analysis of the Markdown source files.

For every change a report is produced listing all of the style or syntax
issues found for that git reference.

For merge requests the diff between the new report and the latest report
for the target branch (e.g. `main`) is rendered in a
[widget](https://git.ligo.org/help/ci/testing/code_quality.html#code-quality-widget)
on the merge request page.

## Special cases for the IGWN Computing Guide

In the majority of cases, all source files should follow the style rules
enforced by `markdownlint` - for better or worse is a standard that should
keep everything consistent.
Some standard is better than no standard.

However, there are a few special cases that have been configured in the
`.markdownlint.yml` file for this project that cater for (a) Duncan's
special brand of pedantry, or (b) a mismatch between `markdownlint` and
`mkdocs` brands of Markdown itself.

### List indentation

- Single-line list items should be indented using a single whitespace
- Multi-paragraph list items should be indented to a total of 4 spaces

The above rules hold for both ordered and unordered lists.
