# metadata
site_name: 'LIGO | Rucio'
site_description: 'Rucio for LIGO'
site_author: 'James Alexander Clark'
site_url: 'https://computing.docs.ligo.org/rucio/guide'
repo_url: 'https://git.ligo.org/computing/rucio/guide'
repo_name: 'GitLab'
edit_uri: 'edit/main/docs'
copyright: 'Copyright &copy; 2023 the KAGRA Collaboration, the LIGO Scientific Collaboration, and the Virgo Collaboration'

# configuration
theme:
  name: 'material_igwn'
  features:
    - navigation.expand
    - navigation.sections
    - navigation.tabs

# content
nav:
  - Home:
    - index.md
  - Administration:
    - Overview: 'admin/index.md'
    - Rucio on NRP k8s: 'admin/nrp.md'
    - Deployment walkthrough: 'admin/walkthrough.md'
    - Service references:
      - Database: 'admin/database.md'
      - Server: 'admin/server.md'
      - Daemons: 'admin/daemons.md'
      - FTS: 'admin/fts.md'
      - Monitoring: 'admin/monit.md'
    - Policy package:
      - Policy package: 'admin/policy.md'
      - LFN-to-PFN algorithms: 'admin/lfn2pfn.md'
  - Configuration:
    - Overview: 'config/index.md'
    - Accounts: 'config/accounts.md'
    - Storage elements: 'config/rses.md'
    - Metadata: 'config/meta.md' 
    - Scopes and names: 'config/scopes.md'
  - Data management & operations: 
    - Overview: 'ops/index.md'
    - GWF file registration:
      - GravCat: 'ops/gravcat.md'
      - Online (k8s) registration: 'ops/online-k8s-registration.md'
      - Online walkthrough: 'ops/online-k8s-walkthrough.md' 
      - Offline registration (HTCondor): 'ops/offline-condor-registration.md'
    - Moving data:
      - Dataset replication: 'ops/replication.md'
      - Transfer configurations: 'ops/transfers.md'
  - Help:
    - Get help: 'help.md'
    - Contribute: 'contributing.md'

# plugins
plugins:
  - search
  - minify:
      minify_html: true
# - redirects:
#     # list of redirects for when pages get renamed
#     redirect_maps:
#       'about.md': 'compsoft/about.md'
#       'authentication.md': 'auth/index.md'

# extensions
markdown_extensions:
  - admonition
  - attr_list
  - footnotes
  - markdown.extensions.codehilite
  - markdown_include.include
  - meta
  - pymdownx.details
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_div_format
  - pymdownx.tabbed:
      alternate_style: true
  - toc:
      permalink: true

# customisation
extra_css:
  - 'assets/stylesheets/computing.css'
extra_javascript:
  - 'https://unpkg.com/mermaid@9.0.1/dist/mermaid.min.js'
