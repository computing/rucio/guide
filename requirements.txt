# base
mkdocs >=1.3.0

# theme
mkdocs-material-igwn

# plugins
markdown-include
mkdocs-material-extensions <1.3
mkdocs-minify-plugin
mkdocs-redirects >=1.0.0
pymdown-extensions

# scripts
python-gitlab
